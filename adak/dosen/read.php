<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <title>Daftar Mahasiswa</title>
</head>
<body>
<div class="container mt-3">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Akademik</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="../index.php">Mata Kuliah <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./dosen/read.php">Dosen</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../mhs/readmhs.php">Mahasiswa</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="card">
    <div class="card-header">
      <span class="my-auto">Daftar Dosen</span>
    </div>
    <div class="card-body">
      <div class="row justify-content-center align-items-center">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Email</th>
            </tr>
          </thead>
          <?php
            include("../../config/koneksi.php");
            $no=0;
            $query = "SELECT * FROM `dosen`";
            $result = $koneksi->query($query);
            while($data = mysqli_fetch_row($result))
            {
            $no++;
          ?>
          <tbody>
            <tr>
            <?php
              echo "<td align=center>$no</td>";
              echo "<td align=center>$data[1]</td>";
              echo "<td align=center>$data[2]</td>";
              echo "<td align=center>$data[3]</td>";
              ?>
              </td>
            </tr>
          </tbody>
          <?php
          }
          ?>
        </table>
      </div>
    </div>
  </div>
  
</div>
<script src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>